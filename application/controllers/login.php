<?php
class login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
	}

	function index() //ini untuk menampilkan halaman login
	{
		$judul="Halaman Login";
		$data['judul']=$judul;
		$this->load->view('login', $data);
	}
	function auth() //ini untuk mengecek login yang ada di database
	{
		$username = $this->input->post('username'); //inputan session itu ibarat kunci untuk masuk 
		$password = $this->input->post('password'); //inputan
		$cek = $this->M_login->login($username,$password); //dikirim ke model
		if ($cek == 1) //jika kondisi benar (1)
		{
			$newdata = array(
				'user' => $username,
				'logged_in' => TRUE
			);
			$this->session->set_userdata($newdata);
			redirect('dashbord'); //diarahkan kehalaman dashboard
		}
		else
		{
			redirect('login'); //diarahkan kehalaman login
			}
		}
	
	function logout() //ini untuk logout
	{
		session_destroy(); //menghapus semua session yang ada destroy (menghapus semua session)
		redirect ('login'); //diarahkan kehalaman login
	}

	function sign(){
		$data['title']="Halaman Login";
		$this->load->view('sign',$data);
	}
}
