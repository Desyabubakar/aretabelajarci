<?php
class Siswa Extends CI_Controller
{
	function __construct()
	{
	parent::__construct();
	$this->load->model('M_siswa');
	}

	function index() // function yang pertama kali di baca
	{
			
	}
	function input()
	{
		$judul="Input Data siswa";
		$data['judul']="$judul";
		//$this->load->view('input_siswa',$data,FALSE);
		$this->template->load('media', 'input_siswa',$data);
	}

	function tampil()
	{
		$judul="Tampil Data siswa";		//ini adalah judul
		$data['judul']="$judul";	//variabel judul
		$data['tampil']=$this->M_siswa->tampil()->result(); //lempar data ke model/menggambil data dari model 
		//$this->load->view('tampil_siswa',$data,FALSE);	//ini untuk menampilkan ke view
		$this->template->load('media', 'tampil_siswa',$data);
	}

	function hapus()
	{
		$id=$this->uri->segment(3); //ambil parameter
		$this->db->where('nim',$id); //parameter dari database
		$this->db->delete('siswa'); //hapus table siswa
		redirect('siswa/tampil'); //menampilkan data siswa
	}
	
	function edit()
	{
		$id=$this->uri->segment(3); //mengambil data dari sekmen 3 nim atau parameter yang ada di url
		$judul="Edit Data siswa";
		$data['judul']="$judul";
		$data['edit']=$this->M_siswa->getId($id)->row_array(); //berfungsi mengambil data dari model
		//$this->load->view('edit_siswa',$data,FALSE);
		$this->template->load('media', 'edit_siswa',$data);
	}
	function simpan()
	{
		$data=array( 
			'nim'=>$this->input->post('nim'), //nim sebelah kanan harus sama dengan nim yang di input.php
			'nama'=>$this->input->post('nama'), //nim sebelah kiri harus sama dengan nim yang di database
			'alamat'=>$this->input->post('alamat'),
			'email'=>$this->input->post('email'),
			);
		$this->M_siswa->simpan($data); //mberfungsi untuk mengirim data ke model
		redirect('siswa/tampil','refresh'); //berfungsi untuk memanggil tampil siswa
	}

	function update()
	{
		$id=$this->input->post('nim'); //ini adalah parameter update
		$data=array( 
			'nim'=>$this->input->post('nim'), 
			'nama'=>$this->input->post('nama'), 
			'alamat'=>$this->input->post('alamat'),
			'email'=>$this->input->post('email'),
			);
		$this->M_siswa->update($data,$id); 
		redirect('siswa/tampil','refresh'); 
	}		
}