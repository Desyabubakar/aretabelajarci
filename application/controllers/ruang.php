<?php
class Ruang Extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_ruang');
	}

	function input()
	{
		$judul="Input Data Ruang";
		$data['judul']="$judul";
		//$this->load->view('input_ruang',$data,FALSE);
		$this->template->load('media', 'input_ruang',$data);
	}

	function tampil()
	{
		$judul="Tampil Data Ruang";
		$data['judul']="$judul";
		$data['tampil']=$this->M_ruang->tampil()->result();
		//$this->load->view('tampil_ruang',$data,FALSE);
		$this->template->load('media', 'tampil_ruang',$data);
	}
	
	function hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('kd_ruang',$id);
		$this->db->delete('ruang');
		redirect('ruang/tampil');
	}

	function edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data Ruang";
		$data['judul']="$judul";
		$data['edit']=$this->M_ruang->getId($id)->row_array();
		//$this->load->view('edit_ruang',$data,FALSE);
		$this->template->load('media', 'edit_ruang',$data);	
	}

	function simpan()
	{
		$data=array(
			'kd_ruang'=>$this->input->post('kode_ruang'),
			'nama_ruang'=>$this->input->post('nama_ruang'),
		);
		$this->M_ruang->simpan($data);
		redirect('ruang/tampil','refresh');
	}

	function update()
	{
		$id=$this->input->post('kode_ruang'); //harus sama dengan di file edit_kelas.php yang diname
		$data=array(
			'kd_ruang'=>$this->input->post('kode_ruang'),
			'nama_ruang'=>$this->input->post('nama_ruang'),
		);
		$this->M_ruang->update($data,$id);
		redirect('ruang/tampil','refresh');
	}
}

