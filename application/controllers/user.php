<?php
class User Extends CI_Controller
{
	function __construct()
	{
	parent::__construct();
	$this->load->model('M_user');
	}

	function index()
	{
			
	}
	function input()
	{
		$judul="Input Data Username";
		$data['judul']="$judul";
		//$this->load->view('input_siswa',$data,FALSE);
		$this->template->load('media', 'input_username',$data);
	}

	function tampil()
	{
		$judul="Tampil Data Username";		//ini adalah judul
		$data['judul']="$judul";	//variabel judul
		$data['tampil']=$this->M_user->tampil()->result(); //lempar data ke model/menggambil data dari model 
		//$this->load->view('tampil_siswa',$data,FALSE);	//ini untuk menampilkan ke view
		$this->template->load('media', 'tampil_username',$data);
	}

	function hapus()
	{
		$id=$this->uri->segment(3); //ambil parameter
		$this->db->where('user',$id); //parameter dari database
		$this->db->delete('user'); //hapus table siswa
		redirect('user/tampil'); //menampilkan data siswa
	}
	
	function edit()
	{
		$id=$this->uri->segment(3); //mengambil data dari sekmen 3 nim atau parameter yang ada di url
		$judul="Edit Data Username";
		$data['judul']="$judul";
		$data['edit']=$this->M_user->getId($id)->row_array(); //berfungsi mengambil data dari model
		//$this->load->view('edit_siswa',$data,FALSE);
		$this->template->load('media', 'edit_username',$data);
	}
	function simpan()
	{
		$data=array( 
			'username'=>$this->input->post('username'), //nim sebelah kanan harus sama dengan nim yang di input.php
			'password'=>$this->input->post('password'), //nim sebelah kiri harus sama dengan nim yang di database
			'nama'=>$this->input->post('nama'),
			'aktif'=>$this->input->post('aktif'),
			'email'=>$this->input->post('email'),
			);
		$this->M_user->simpan($data); //mberfungsi untuk mengirim data ke model
		redirect('user/tampil','refresh'); //berfungsi untuk memanggil tampil siswa
	}

	function update()
	{
		$id=$this->input->post('user'); //ini adalah parameter update
		$data=array( 
			'username'=>$this->input->post('username'), 
			'password'=>$this->input->post('password'), 
			'nama'=>$this->input->post('nama'),
			'aktif'=>$this->input->post('aktif'),
			'email'=>$this->input->post('email'),
			);
		$this->M_user->update($data,$user); 
		redirect('user/tampil','refresh'); 
	}		
}





