<?php
class Mapel Extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_mapel');
	}

	function input()
	{
		$judul="Input Data Mapel";
		$data['judul']="$judul";
		//$this->load->view('input_mapel',$data,FALSE);
		$this->template->load('media', 'input_mapel',$data);
	}

	function tampil()
	{
		$judul="Tampil Data Mapel";
		$data['judul']="$judul";
		$data['tampil']=$this->M_mapel->tampil()->result();
		//$this->load->view('tampil_mapel',$data,FALSE);
		$this->template->load('media', 'tampil_mapel',$data);
	}

	function hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('kd_mapel',$id);
		$this->db->delete('mapel');
		redirect('mapel/tampil');
	}

	function edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data Mapel";
		$data['judul']="$judul";
		$data['edit']=$this->M_mapel->getId($id)->row_array();
		//$this->load->view('edit_mapel',$data,FALSE);	
		$this->template->load('media', 'edit_mapel',$data);
	}

	function simpan()
	{
		$data=array(
			'kd_mapel'=>$this->input->post('kode_mapel'),
			'nama_mapel'=>$this->input->post('nama_mapel'),
			'sks'=>$this->input->post('sks'),
		);
		$this->M_mapel->simpan($data);
		redirect('mapel/tampil','refresh');
	}

	function update()
	{
		$id=$this->input->post('kode_mapel');
		$data=array(
			'kd_mapel'=>$this->input->post('kode_mapel'),
			'nama_mapel'=>$this->input->post('nama_mapel'),
			'sks'=>$this->input->post('sks'),
		);
		$this->M_mapel->update($data,$id);
		redirect('mapel/tampil','refresh');
	}
}

