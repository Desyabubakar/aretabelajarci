<?php
class M_ruang extends CI_Model {

	function tampil()
	{
		$ruang=$this->db->get('ruang');
		return $ruang;
	}

	function simpan($data)
	{
		$this->db->insert('ruang',$data);
	}

	function getId($data)
	{
		$param=array('kd_ruang'=>$data);
		return $this->db->get_where('ruang',$param);
	}

	function update($data,$id)
	{
		$this->db->where('kd_ruang',$id);
		$this->db->update('ruang',$data);
	}

}