<?php  
class M_siswa extends CI_Model {

	function tampil()
	{
		$siswa=$this->db->get('siswa'); //ini sama dengan select * from siswa kalau di CMD 
		return $siswa; //2 baris menampilkan database

		//$siswa2=$this->db->query("select * from siswa"); //ini query biasa
	}

	function simpan($data)
	{
		$this->db->insert('siswa',$data);
	}

	function getId($data)
	{
		$param=array('nim'=>$data);
		return $this->db->get_where('siswa',$param);
	}

	function update($data,$id)
	{
		$this->db->where('nim',$id);
		$this->db->update('siswa',$data);
	}
}