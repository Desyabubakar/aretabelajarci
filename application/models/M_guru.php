<?php
class M_guru extends CI_Model {

	function tampil()
	{
		$guru=$this->db->get('guru'); //ini sama dengan select * from siswa kalau di CMD 
		return $guru; //2 baris menampilkan database
	}

	function simpan($data)
	{
		$this->db->insert('guru',$data);
	}

	function getId($data)
	{
		$param=array('nik'=>$data);
		return $this->db->get_where('guru',$param);
	}

	function update($data,$id)
	{
		$this->db->where('nik',$id);
		$this->db->update('guru',$data);
	}
}