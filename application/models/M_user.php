<?php  
class M_user extends CI_Model {

	function tampil()
	{
		$user=$this->db->get('user'); //ini sama dengan select * from siswa kalau di CMD 
		return $user; //2 baris menampilkan database
	}

	function simpan($data)
	{
		$this->db->insert('user',$data);
	}

	function getId($data)
	{
		$param=array('user'=>$data);
		return $this->db->get_where('user',$param);
	}

	function update($data,$id)
	{
		$this->db->where('user',$id);
		$this->db->update('user',$data);
	}
}