<?php 
if (empty($this->session->userdata('user'))){ //jika session kosong
  redirect('login'); // diarahkan ke halaman login
}
else // jika session tidak kosong maka menampilkan template
{
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Desy Bakar</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url()?>Assets/css/bootstrap.min.css">
  <script src="<?php echo base_url()?>Assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url()?>Assets/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
  </style>
</head>
<body>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h2>Menu Admin</h2>
      <ul class="nav nav-pills nav-stacked">
    <li class="active"><a href="<?php echo base_url(); ?>/index.php/login">Login</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>/index.php/dashbord">Dashboard</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>/index.php/user/tampil">Username</a></li>
		<li class="active"><a href="<?php echo base_url(); ?>/index.php/siswa/tampil">Siswa</a></li>
		<li class="active"><a href="<?php echo base_url(); ?>/index.php/guru/tampil">Guru</a></li>
		<li class="active"><a href="<?php echo base_url(); ?>/index.php/mapel/tampil">Mapel</a></li>
		<li class="active"><a href="<?php echo base_url(); ?>/index.php/ruang/tampil">Ruang</a></li>
		<li class="active"><a href="<?php echo base_url(); ?>/index.php/kelas/tampil">Kelas</a></li>
		<li class="active"><a href="<?php echo base_url(); ?>/index.php/jadwal/tampil">Jadwal</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>/index.php/login/logout">Logout</a></li>
		
      </ul><br>
      <div class="input-group">
           
          </button>
        </span>
      </div>
    </div>

    <div class="col-sm-9">
         
      <h3><b>Artikel Anda Disini</b></h3>
	  <hr>
    <p><?php echo $contents;?></p>
      
      
      
   
    </div>
  </div>
</div>

<footer class="container-fluid">
  <p>Footer Text</p>
</footer>

</body>
</html>
<?php 
}
?>